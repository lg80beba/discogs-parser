#!/usr/bin/python
# -*- coding: utf8 -*-

"""
Author: Lukas Gienapp
Version: 0.1
Date: 12.01.2019
"""
from lxml import etree
import networkx as nx
import datetime
import re
import sys
import json

# Command line options
args = sys.argv
FILENAME_RELEASES = args[1]
FILENAME_NODES = args[2]
FILENAME_CONFIG = args[3]

# Config file parsing
with open(FILENAME_CONFIG,"r") as file:
	config = json.load(file)
ROLES = config['roles']
GENRES = config['genres']
STRICT = config['strict'] # STRICT == False not implemented yet

# Regex patterns
optional_info_regex = re.compile(r'\[.*?\]')

# Initializing graphs
graphs = {}
for genre in GENRES:
	graphs[genre] = nx.DiGraph()

# Parsing release file (building graphs)
for event, element in etree.iterparse(FILENAME_RELEASES, tag="release"):
	release_id = int(element.attrib['id'])

	if element.attrib['status'] != 'Accepted':
		element.clear()
		continue

	quality = element.find('data_quality').text
	if quality not in ['Complete and Correct', 'Correct']:
		element.clear()
		continue

	try:
		genres =  [x.text if x != None else '' for x in element.find('genres').findall('genre')]
	except:
		element.clear()
		continue
	if (STRICT and len(genres) == 1):
		# Genre
		genre = genres[0]	
		if genre not in GENRES:
			element.clear()
			continue
		# Date
		try:
			date = datetime.datetime.strptime(element.find('released').text, '%Y-%m-%d')
		except ValueError:
			date = datetime.datetime(1, 1, 1, 0, 0)
		except AttributeError:
			date = datetime.datetime(1, 1, 1, 0, 0)
		finally:
			year = date.year	

		# Artists
		artists = [(artist.find('id').text, 'Main Artist') for artist in element.find('artists').findall('artist')]

		# Relations
		extras = []
		for artist in element.find('extraartists').findall('artist'):
			extra_id = artist.find('id').text
			extra_roles = (
				{x.strip() for x in re.sub(optional_info_regex, '', artist.find('role').text).split(',')}
				.intersection(ROLES)
			)
			if len(extra_roles) == 0:
				element.clear()
				continue
			for role in extra_roles:	
				extras.append((extra_id, role))

		# Add relations to respective genre graph	
		for from_id, role_from in extras+artists:
			for to_id, role_to in extras+artists:
				if from_id == to_id:
					continue
				else:
					graphs[genre].add_edge(int(from_id),int(to_id),start=year,Label=role_from,release_id=release_id)
		
	element.clear()

# Parse artists file (adding node metadata)
for genre in graphs.keys(): 
	nx.set_node_attributes(graphs[genre], ' ', 'Label') # Empty Metadata to create dict key

for event, element in etree.iterparse(FILENAME_NODES, tag="artist"):
	artist_id = int(element.find('id').text)
	for genre in GENRES:
		if artist_id in graphs[genre].nodes:
			graphs[genre].node[artist_id]['Label'] = element.find('name').text # Add actual name	
	element.clear()

# Save graphs
for genre in graphs.keys(): 
	filename = (
		genre
		.lower()
		.replace(" ","")
		.replace("/","")
		.replace(",","")
		.replace("&","")
	)
	nx.write_graphml(graphs[genre], filename+".graphml")
